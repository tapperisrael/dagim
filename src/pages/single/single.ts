import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {SendToServerService} from "../../services/send_to_server";
import {BasketService} from "../../services/basket.service";
import {RegisterPage} from "../register/register";
import {Config} from "../../services/config";
import {BasketPage} from "../basket/basket";
import {PopupService} from "../../services/popups";
import {CategoriesPage} from "../categories/categories";
import {LangChangeEvent, TranslateService} from "ng2-translate";

/**
 * Generated class for the SinglePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-single',
    templateUrl: 'single.html',
})
export class SinglePage {
    
    public ProductIndex = '';
    public CategoryId = '';
    public SubCatId = '';
    public Product = '';
    public Qnt = 1;
    public productId = '';
    public Price = '';
    public ServerHost;
    public basket;
    public BasketPrice;
    public BasketLen;
    public defaultLangage = '';
    public Type;
    public Fav:number = 0;
    public AC1:string = '';
    public AC2:string = ''
    public answer = 'תשובה לשאלה מספר 1'
    
    
    constructor(public navCtrl: NavController, public navParams: NavParams, public Server: SendToServerService, private alertCtrl: AlertController, public Settings: Config, public BasketService: BasketService, public Popup: PopupService, private translate: TranslateService) {
        
        translate.onLangChange.subscribe((event: LangChangeEvent) => {
        
        });
        
        /*    this.ServerHost = this.Settings.ServerHost;
            this.SubCatId = this.navParams.get('subcat');
            this.ProductIndex = this.navParams.get('id');
            this.products = this.Server.SubCategories[this.SubCatId].cat_products[this.Id ];
            console.log ("products:" , this.products)
      */
        this.Type = navParams.get('type');
        this.ServerHost = this.Settings.ServerHost;
        
        if (this.Type == 0) {
            this.ProductIndex = navParams.get('id');
            this.CategoryId = navParams.get('cat');
            this.SubCatId = navParams.get('subcat');
            this.Product = this.Server.SubCategories[this.SubCatId].cat_products[this.ProductIndex];
        }
        
        if (this.Type == 1) {
            this.Product = navParams.get('product');
        }
        this.defaultLangage = this.Settings.defaultLanguage;
        this.AC1 = 'closed';
        this.AC2 = 'closed';
        if (this.BasketService.basket.length > 0)
        this.BasketLen = this.BasketService.basket.length;
        console.log("Product : ", this.BasketService.basket)
    }
    
    ionViewDidLoad() {
        console.log('ionViewDidLoad SinglePage');
    }
    
    
    goback() {
        this.navCtrl.pop();
        console.log('Click on button Test Console Log');
    }
    
    AddToBasket() {

        this.Price = this.Product['high_price'];
        this.BasketService.AddProductsProducts(this.productId, this.Qnt, this.Price, this.Product['title'], this.Product['title_english'], this.Product['image']);
        this.presentConfirm();
    }
    
    
    presentConfirm() {
        let alert = this.alertCtrl.create({
            title: this.translate.instant("prdaddcattext"),
            message: this.translate.instant("continueshoppingconfirmtext"),
            buttons: [
                {
                    text: this.translate.instant("continueshoppingtext"),
                    role: 'cancel',
                    handler: () => {
                        this.goToCategoriesPage();
                    }
                },
                {
                    text: this.translate.instant("movebaskettext"),
                    handler: () => {
                        this.goToBasket();
                    }
                }
            ]
        });
        alert.present();
    }
    
    CalculateBasket() {
        this.BasketPrice = this.BasketService.CalculateBasket();
    }
    
    goToBasket() {
        this.navCtrl.push(BasketPage);
    }
    
    goToCategoriesPage() {
        this.navCtrl.push(CategoriesPage);
    }
    
    GoToNextPage() {
        this.navCtrl.push(RegisterPage);
    }
    
    changeFav()
    {
        if(this.Fav == 0)
            this.Fav = 1;
        else
            this.Fav = 0;
    }
    
    
    openItem () {
        this.AC1 = this.AC1 === 'closed' ? 'expanded' : 'closed';
    }
    
    openItem1 () {
        this.AC2 = this.AC2 === 'closed' ? 'expanded' : 'closed';
    }
    
    
}
