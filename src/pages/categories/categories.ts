import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ShopPage} from "../shop/shop";
import {SendToServerService} from "../../services/send_to_server";
import {Config} from "../../services/config";
import {SubcategoryPage} from "../subcategory/subcategory";
import {SinglePage} from "../single/single";

/**
 * Generated class for the CategoriesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-categories',
    templateUrl: 'categories.html',
})
export class CategoriesPage {

    public ProductsArray;
    public ServerHost;
    public SubCategories: any[] = [];
    public Products: any[] = [];
    public isAvailble = false;
    public CurrentSubCat = 0;
    public CategoryId = 5;

    constructor(public navCtrl: NavController, public navParams: NavParams, public Server: SendToServerService, public Settings: Config) {
        this.ServerHost = this.Settings.ServerHost;
        this.ProductsArray = this.Server.ProductsArray;
        this.Server.GetSubCategoriesById('GetSubCategoriesById', this.CategoryId).then((data: any) => {
            this.SubCategories = data;
            this.getProductsArray(this.CurrentSubCat);
            console.log("SubCategories : ", data);
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CategoriesPage');
    }

    gotToShopPage(index) {
       // this.navCtrl.push(SubcategoryPage, {id: index});
        this.navCtrl.push(SinglePage, {cat:this.CategoryId,subcat:this.CurrentSubCat,id:index,type:0});
    }

    getProductsArray(id)
    {
        this.CurrentSubCat = id;
        this.Products = this.SubCategories[id].cat_products;
        this.isAvailble = true;
        console.log("PR : " ,this.Products )
    }
}
