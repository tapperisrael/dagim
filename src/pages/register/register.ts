import {Component, OnInit} from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams, ViewController} from 'ionic-angular';
import {BasketPage} from "../basket/basket";
import {Config} from "../../services/config";
import {SendToServerService} from "../../services/send_to_server";
import {BasketService} from "../../services/basket.service";
import {PopupPage} from "../popup/popup";
import {HomePage} from "../home/home";
import {PopupService} from "../../services/popups";
import {LangChangeEvent, TranslateService} from "ng2-translate";

/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage implements OnInit{

    public products;
    public isAvaileble = false;
    public TotalPrice = 0;
    public ServerHost;
    public emailregex;
    public serverResponse;

  public Details = {
      'name':'',
      'mail':'',
      'password':'',
      'address':'',
      'phone':'',
      'newsletter' : false
  }


  constructor(public navCtrl: NavController, public navParams: NavParams , public Settings:Config , public alertCtrl:AlertController, public Server:SendToServerService,public BasketService:BasketService,public modalCtrl: ModalController, public Popup:PopupService, private translate: TranslateService) {

      translate.onLangChange.subscribe((event: LangChangeEvent) => {

      });

      translate.setDefaultLang('he');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

    ngOnInit()
    {

        this.ServerHost = this.Settings.ServerHost;
        this.products = this.BasketService.basket;
        this.TotalPrice = 0;

        for (var i = 0; i < this.products.length; i++) {
            this.TotalPrice += Number(this.products[i].Qnt) * Number(this.products[i].price);
        }
        console.log("cart:" , this.products);
    }

  doRegister()
  {
     this.emailregex = /\S+@\S+\.\S+/;
    if(this.Details.name.length < 3)
        this.Popup.presentAlert( this.translate.instant("missingFieldsText") ,this.translate.instant("requiredFullnameText"),'')

    else if(this.Details.address.length < 3)
          this.Popup.presentAlert( this.translate.instant("missingFieldsText") ,this.translate.instant("missingaddresstext"),'')

    else if(this.Details.mail =="")
        this.Popup.presentAlert( this.translate.instant("missingFieldsText") ,this.translate.instant("requiredEmailText"),'')

    else if (!this.emailregex.test(this.Details.mail)) {
        this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("requiredEmailText"),'');
        this.Details.mail = '';
    }

      else if(this.Details.password =="")
          this.Popup.presentAlert( this.translate.instant("missingFieldsText") ,this.translate.instant("missingpasswordtext"),'')

    else
    {
            this.serverResponse = this.Server.RegisterUser("RegisterUser",this.Details).then(data=>{
            console.log("register response: ",  data);

            if (data[0].status == 0) {
                this.Popup.presentAlert(this.translate.instant("missingFieldsText"), this.translate.instant("mailalreadyexiststext"),'');
                this.Details.mail = '';
            }
            else {
                window.localStorage.userid = data[0].userid;
                window.localStorage.name = this.Details.name;
                this.Details.name = '';
                this.Details.mail = '';
                this.Details.address = '';
                this.Details.phone = '';
                this.Details.newsletter = false;
                this.navCtrl.push(BasketPage);
            }
        });
    }
  }



}
