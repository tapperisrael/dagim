import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {SendToServerService} from "../../services/send_to_server";
import {BasketPage} from "../basket/basket";
import {RegisterPage} from "../register/register";
import {ForgotPage} from "../forgot/forgot";
import {PopupService} from "../../services/popups";
import {LangChangeEvent, TranslateService} from "ng2-translate";

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */



@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public Login =
  {
    "mail" : "",
    "password" : ""

  }
  public emailregex;
  public loginStatus;
  public serverResponse;

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl:AlertController, public Server:SendToServerService, public Popup:PopupService, private translate: TranslateService) {
      translate.setDefaultLang('he');
     translate.onLangChange.subscribe((event: LangChangeEvent) => {

     });

  }

    loginUser()
    {
      this.emailregex = /\S+@\S+\.\S+/;

      if (this.Login.mail =="")
        this.Popup.presentAlert(this.translate.instant("requiredtext"),this.translate.instant("inputmailtext"),'');

      else if (!this.emailregex.test(this.Login.mail)) {
          this.Popup.presentAlert(this.translate.instant("requiredtext"), this.translate.instant("requiredEmailText"),'');
          this.Login.mail= '';
      }
      else if (this.Login.password =="")
          this.Popup.presentAlert(this.translate.instant("requiredtext"),this.translate.instant("missingpasswordtext"),'');
      else
        {
           this.serverResponse = this.Server.LoginUser("UserLogin",this.Login).then(data=>{
           console.log("login response: ",  data);
            if (data.length == 0) {
                this.Popup.presentAlert(this.translate.instant("badlogintitle"), this.translate.instant("bademailorpasswordtext"),'');
                this.Login.password = '';
            }
            else{
                window.localStorage.userid = data[0].id;
                window.localStorage.name = data[0].name;
                this.Login.mail= '';
                this.Login.password= '';
                this.navCtrl.push(BasketPage);
            }

           });
        }

    }

    goRegisterPage()
    {
        this.navCtrl.push(RegisterPage);
    }

    goForgotPage()
    {
        this.navCtrl.push(ForgotPage);
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


}
