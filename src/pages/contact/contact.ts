import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {HomePage} from "../home/home";
import {Config} from "../../services/config";
import {SendToServerService} from "../../services/send_to_server";
import {PopupService} from "../../services/popups";
import {LangChangeEvent, TranslateService} from "ng2-translate";

/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

    public Contact = {
        'name':'',
        'mail':'',
        'details':'',
        'phone':'',
    }



  constructor(public navCtrl: NavController, public navParams: NavParams , public alertCtrl:AlertController , public Settings:Config ,  public Server:SendToServerService, public Popup:PopupService, private translate: TranslateService){

       translate.onLangChange.subscribe((event: LangChangeEvent) => {

      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }

  SendForm()
  {

      if(this.Contact.name.length < 3)
          this.Popup.presentAlert(this.translate.instant("missingFieldsText") ,this.translate.instant("requiredFullnameText"), 0)

      else if(this.Contact.mail.length < 3)
          this.Popup.presentAlert( this.translate.instant("missingFieldsText") , this.translate.instant("requiredEmailText"), 0)

      else if(this.Contact.phone.length < 3)
          this.Popup.presentAlert( this.translate.instant("missingFieldsText") ,this.translate.instant("requiredPhonetext"), 0)

      else if(this.Contact.details.length < 3)
          this.Popup.presentAlert( this.translate.instant("missingFieldsText") ,this.translate.instant("requiredDescText"), 0)
      else
      {
          this.Settings.ContactDetails = this.Contact;
          this.Server.sendContactDetails('getContact');
          this.Popup.presentAlert( this.translate.instant("sentOkTitleText") ,this.translate.instant("SendOktextText"), 1,this.RedirectFun())
      }

  }

    RedirectFun()
    {
        this.navCtrl.push(HomePage);
    }

}
