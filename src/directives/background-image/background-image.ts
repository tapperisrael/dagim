import { Directive, ElementRef, Input, SimpleChanges } from '@angular/core';

@Directive({
    selector: '[background-image]' // Attribute selector
})
export class BackgroundImageDirective {
    
    @Input()
    source: string;
    
    constructor(private el: ElementRef) {}
    
    ngOnChanges(changes: SimpleChanges) {
        for (let key in changes) {
            if (key == 'source') {
                if (this.source != null && this.source != undefined) {
                    this.el.nativeElement.style.background = `url('${this.source}') no-repeat top left / cover`;
                }
            }
        }
    }
    
}