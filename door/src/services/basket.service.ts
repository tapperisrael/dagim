
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";


@Injectable()

export class BasketService
{
    public basket:any[] = [];
    public Obj = new Object();
    public BasketTotalPrice = 0;
    public productFound = 0;
    _BasketTotalPrice: Subject<number> = new Subject();
    BasketTotalPrice$: Observable<number> = this._BasketTotalPrice.asObservable();

    constructor(private http:Http,public Settings:Config) { };

    AddProductsProducts(productId,Qnt,price,name,nameenglish,image)
    {
        // console.log("Add : " + )
        // this.productFound = 0;
        // if (this.basket.length > 0)
        // {
        //     for (var i = 0; i < this.basket.length; i++) {
        //         if (this.basket[i].id == productId)
        //         {
        //             this.productFound = 1;
        //             this.basket[i].Qnt += Number(Qnt);
        //             //this.basket[i].Pay +=  Number(Pay);
        //         }
        //     }
        // }
        //
        // if (this.productFound == 0)
        // {
            this.Obj = {
                "id":productId,
                "Qnt":Number(Qnt),
                //"Pay":Number(Pay),
                "price":price,
                "name":name,
                "name_english":nameenglish,
                "image":image
            }

            this.basket.push(this.Obj);
       // }

        this.CalculateBasket();
        window.localStorage.basket = JSON.stringify(this.basket);
        console.log("Basket : " , this.basket)
    }

    CalculateBasket() {
        this.BasketTotalPrice = 0;
        for (var i = 0; i < this.basket.length; i++) {
            this.BasketTotalPrice += parseInt(this.basket[i].price) * this.basket[i].Qnt;
            console.log("CalculateBasket", this.BasketTotalPrice)
        }

        this.setPrice(this.BasketTotalPrice);
    }

    setPrice (value) {

        this.BasketTotalPrice = value;
        this._BasketTotalPrice.next(value);

    }

    resetTotalPrice()
    {
        this.BasketTotalPrice = 0;
    }

    emptyBasket()
    {
        console.log("EMPTY")
        this.basket = [];
        window.localStorage.basket = ''
    }

    deleteProduct(index)
    {
        this.basket.splice(index, 1);
        this.CalculateBasket();
    }

};


