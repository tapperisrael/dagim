import {Component , Input } from '@angular/core';
import {Config} from "../../services/config";
import {BasketService} from "../../services/basket.service";
import {NavController} from "ionic-angular";
import {BasketPage} from "../../pages/basket/basket";

/**
 * Generated class for the MainHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
    selector: 'main-header',
    templateUrl: 'main-header.html'
})
export class MainHeaderComponent {
    text: string;
    BasketTotalPrice = 0;
    Back = '';

    constructor(public navCtrl: NavController , public Settings: Config, public Basket: BasketService) {
       this.BasketTotalPrice = Basket.BasketTotalPrice;
    }

    goBack()
    {
        console.log(1)
    }

    goToBasket()
    {
        this.navCtrl.push(BasketPage);
    }

}
