import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {AboutPage} from "../pages/about/about";
import {SendToServerService} from "../services/send_to_server";
import {Http, HttpModule} from "@angular/http";
import {Config} from "../services/config";
import {ShopPage} from "../pages/shop/shop";
import {SinglePage} from "../pages/single/single";
import {BasketPage} from "../pages/basket/basket";
import {BasketService} from "../services/basket.service";
import {RegisterPage} from "../pages/register/register";
import {FooterComponent} from "../components/footer/footer";
import {HeaderComponent} from "../components/header/header";
import {ContactPage} from "../pages/contact/contact";
import {SocialSharing} from '@ionic-native/social-sharing';
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {PopupPage} from "../pages/popup/popup";
import {CategoriesPage} from "../pages/categories/categories";
import {LoginPage} from "../pages/login/login";
import {ForgotPage} from "../pages/forgot/forgot";
import {PopupService} from "../services/popups";
import {UserService} from "../services/user";
import {TranslateLoader, TranslateModule, TranslateStaticLoader} from 'ng2-translate';
import {SubcategoryPage} from "../pages/subcategory/subcategory";
import {DirectivesModule} from "../directives/directives.module";
import {MainHeaderComponent} from "../components/main-header/main-header";


@NgModule({
    declarations: [
        MyApp,
        HomePage,
        AboutPage,
        ShopPage, SinglePage, BasketPage,
        RegisterPage,
        FooterComponent,
        HeaderComponent,
        MainHeaderComponent,
        ContactPage,
        PopupPage,
        CategoriesPage,
        LoginPage,
        ForgotPage,
        SubcategoryPage
    ],
    imports: [
        BrowserModule,
        DirectivesModule,
        IonicModule.forRoot(MyApp),
        HttpModule,
        TranslateModule.forRoot({
            provide: TranslateLoader,
            useFactory: (http: Http) => new TranslateStaticLoader(http, '/assets/i18n', '.json'),
            deps: [Http]
        })],
    exports: [BrowserModule, HttpModule, TranslateModule],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        AboutPage,
        ShopPage,
        SinglePage,
        BasketPage,
        RegisterPage,
        FooterComponent,
        HeaderComponent,
        MainHeaderComponent,
        ContactPage,
        PopupPage,
        CategoriesPage,
        LoginPage,
        ForgotPage,
        SubcategoryPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        SendToServerService,
        Config,
        BasketService,
        SocialSharing,
        InAppBrowser,
        PopupService,
        UserService,
    ]
})
export class AppModule {
}
